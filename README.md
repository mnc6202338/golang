● Jenis struktur proyek/folder/kode seperti apa yang biasanya kamu terapkan dalam
membangun sebuah layanan aplikasi (misal: API), gunakan todo app sederhana sebagai
dasar, atau kamu juga dapat menggunakan proyek (pengalaman) sebelumnya sebagai
referensi.
- Dengan Menggunakan Golang

● Buat fungsi sederhana untuk dapat berhubungan ke database remote (ambil postgresql
sebagai contoh)
- Saya menggunakan MongoDb Cloud

● Buat fungsi sederhana untuk dapat berhubungan ke layanan eksternal (misalnya google
cloud storage)
- Saya menggunakan MongoDb Cloud

● Jelaskan bagaimana cara kamu menyusun dan mengabstraksi error-handling?
- yang pertama error handling membaca file .env dikarenakan untuk mendeteksi koneksi ke arah mongoDB Berjalan/Tidak, jika tidak, maka Error handling bisa keluar error nya. 
- Setelah .env error handling dipasang mendeteksi port yang dijalankan ada conflict/tidaknya. 

● Jelaskan bagaimana kamu akan memisahkan dan atau melakukan abstraksi terhadap suatu
proyek, gunakan todo app sederhana sebagai dasar, atau kamu juga dapat menggunakan
proyek (pengalaman) sebelumnya sebagai referensi.
- Hal pertama memisahkan router dengan main.go, supaya bisa memanage dengan mudah
- Hal kedua membuat model, file ini untuk membuat/manage tabel yang sudah konek di mongoDB
- Hal ketiga membuat middleware untuk membuat query yang di butuhkan untuk konek di MongoDB
dari ketiga hal tersebut bisa di fungsikan untuk memudahkan pencarian problem solving suatu projek.
